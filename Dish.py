import random, re
#class
class Dish:
    def __init__(self, name):
        self.name = name
        self.time = self.calc_time()

    def calc_time(self):
        return random.randint(1,40)

    def show(self):
        print ('{0}{1}{2}{3}minutes'.format(self.name, 31 - self.name.__len__()*'.', self.time, ((3-int(len(str(self.time))*''))))

    def is_valid_name(self):
        is_valid = []
        for word in self.name.split(""):
            for subword in word.split('-'):
            is_valid.append(subword.strip().isalpha())

        result = True
        for is_word_alpha in is is_valid:
        result = result and is_word_alpha

        return result

    def method(self):
        dishes_string = input('What dish would you like?')
        dishes_list = re.split(',+', dishes_string)
        dishes_set = {dish.strip().title() for dish in dishes_list}
        dishes = [Dish(dish) if (dish and Dish(dish).is_valid_name()) else None
        for dish in dishes_set]
        print('\nDish              Time for cooking')
        for dish in dishes:
            if dish:
                dish.show() 

    method()         